#include <ESP8266WiFi.h>
#include <Artnet.h>
#include <Servo.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <Wire.h>
#include <HMC5883L.h>


Artnet artnet;
Servo panServo;//drehung
Servo tiltServo;//schwenken
WiFiClient debug;
HMC5883L compass;

int initState = 0;//0 startup; 1; wlan searching 2 wlan up; 3artnet up

const char* ssid = "wifiname";
const char* pw = "wifipw";
int dmxChannel = 0;
int dmxUniverse = 2;

const int sclPin = D6;
const int sdaPin = D7;

float destHeading  = PI;
int foundHeading = false;

void setup()
{
  panServo.attach(D1);
  panServo.write(90);
  tiltServo.attach(D2);

  analogWriteFreq(500);
  pinMode(D3, OUTPUT);
  pinMode(D4, OUTPUT);
  pinMode(D5, OUTPUT);
  analogWrite(D3, 0);
  analogWrite(D4, 0);
  analogWrite(D5, 0);
  
  
  Serial.begin(9600);
  //Serial.print("startup...  ");
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, pw);

  Wire.begin(sdaPin, sclPin);
  compass = HMC5883L();
  int err = 0;
  compass.SetScale(1.3);
  err = compass.SetMeasurementMode(Measurement_Continuous);
  if(err != 0){
    Serial.println(compass.GetErrorText(err));
    initState = 4;
    return;
  }

  initState++;
}


void loop()
{

  if (initState == 1) {
    // Wait for connection
    if (WiFi.status() == WL_CONNECTED) {
      Serial.print("IP address: ");
      Serial.println(WiFi.localIP());

      //OTA
      ArduinoOTA.setHostname("movinghead");
      ArduinoOTA.onStart([]() {
        Serial.println("Start");
      });
      ArduinoOTA.onEnd([]() {
        Serial.println("\nEnd");
      });
      ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
        Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
      });
      ArduinoOTA.onError([](ota_error_t error) {
        Serial.printf("Error[%u]: ", error);
        if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
        else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
        else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
        else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
        else if (error == OTA_END_ERROR) Serial.println("End Failed");
      });
      ArduinoOTA.begin();

      debug.connect("192.168.189.54", 6789);
      debug.print("hello!");
              
        initState++;
      }
    }
  else if (initState == 2) {
    artnet.begin();
    byte brodcast[4];
    brodcast[0] = WiFi.localIP()[0];
    brodcast[1] = WiFi.localIP()[1];
    brodcast[2] = WiFi.localIP()[2];
    brodcast[3] = 255;
    artnet.setBroadcast(brodcast);
    artnet.setUniverseA(dmxUniverse);


    Serial.println("init finished");
    initState++;
  }
  else if(initState == 3) {
    uint16_t r = artnet.read();
    if (r == ART_DMX)
    {
      if (artnet.getUniverse() == dmxUniverse) {
        //panServo.write((float(artnet.getDmxFrame()[dmxChannel])/255.0f)*180.0);
        float newDestHeading = (float(artnet.getDmxFrame()[dmxChannel])/255.0f) * PI * 2.0;
        if(newDestHeading != destHeading){
          destHeading = newDestHeading;
          foundHeading = false;
        }
        
        tiltServo.write((float(artnet.getDmxFrame()[dmxChannel+1])/255.0f)*180.0);
        analogWrite(D5, (float(artnet.getDmxFrame()[dmxChannel+2])/255.0f)*900.0);
        analogWrite(D4, (float(artnet.getDmxFrame()[dmxChannel+3])/255.0f)*900.0);
        analogWrite(D3, (float(artnet.getDmxFrame()[dmxChannel+4])/255.0f)*900.0);
      }
    }


    //kompass
    MagnetometerRaw raw = compass.ReadRawAxis();
    MagnetometerScaled scaled = compass.ReadScaledAxis();
    int MilliGauss_OnThe_XAxis = scaled.XAxis;// (or YAxis, or ZAxis)
    float heading = atan2(int(scaled.YAxis), int(scaled.XAxis));
    if(heading < 0)
      heading += 2*PI;

    float headingDegrees = heading * 180/M_PI; 

    float diff = heading - destHeading;
    
    if(diff > PI) diff -= 2* PI;
    if(diff < -PI) diff += 2* PI;

    if(foundHeading){
      diff = 0;
    }
    else{
      float absDiff = diff;
      if(absDiff < 0)
        absDiff *= -1;
      if(absDiff < M_PI * 0.03){
        foundHeading = true;
      }
    }
  
    float speed = diff / (PI /3.0);
    if(speed > 1) speed = 1;
    if(speed < -1) speed = -1;

    panServo.write(int(87.0 + 50.0 * speed));

    char out [50];
    sprintf(out,"headiung: %i    dest: %i   speed: %i   tilt %i", int(headingDegrees),int((destHeading/PI) * 180.0 ), int(speed * 100.0), int(artnet.getDmxFrame()[dmxChannel+1]));
    debug.print(out);
    
    ArduinoOTA.handle();
  }
  else if(initState == 4){//error
        analogWrite(D5, 1023);
        analogWrite(D4, 0);
        analogWrite(D3, 0);
        panServo.write(90);
        ArduinoOTA.handle();
  }
  
}

