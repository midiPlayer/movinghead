# DIY-Movinghead
![case](./movinghead.jpg)

## Software
### Arduino-Librarys:
- [Kompass](https://github.com/manifestinteractive/arduino/tree/master/Libraries/HMC5883L)
- [Artnet for ESP](https://github.com/nickysemenza/Artnet)

## Hardware
lasercutt out of 4mm wood. See [parts.svg](parts.svg)
When opening in inkscape set Display -> Displaymode -> Outline in order to see anything.

For all other components look at the [wiki](https://gitlab.com/midiPlayer/movinghead/wikis/Teile)